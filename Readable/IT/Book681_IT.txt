Cari signor Curve, signorina Talochard, signorina Paimon e {M#signore}{F#signora} {NICKNAME},
Ci siamo trasferiti, però pensavamo che sareste tornati, quindi vi abbiamo lasciato questo bigliettino. Mamma dice che per ora dobbiamo tenere segreto il nostro nuovo indirizzo, quindi ve lo darò la prossima volta. Eheh.
Grazie a voi, ragazzi, credo di potere affermare che questa volta ho trovato la risposta che cercavo.
Ah, sembra che il signor Curve sia proprio incapace di mentire! Lo ammiro. Sarà sempre come un fratellone per me!