This file contains data on the structures contained within the `ExcelBinOutput` directory, that I use the most.
I'll later add a file with less-used structures, as well.


`AchievementExcelConfigData.json`
----------------------------------
This is a list of achievements.
Each achievement is represented by a dict of the following format:

```
  {
    "goalId": 17,
    # "goalID" corresponds to "id" in `AchievementGoalExcelConfigData.json`
    # which is the name of the category of achievements
    # for example, 17 is "Memories of the Heart"

    "orderId": 6010,
    # this is the order in which it appears in the achievement UI in game

    "titleTextMapHash": 620492212,
    # this is the foreign key to the title of the achievement
    # "nameTextMapHash" corresponds to the keys in `TextMapEN.json` (or other languages)
    # in this case, the corresponding record is "620492212": "Archaic Lord of Lightning and Blitz"

    "descTextMapHash": 718294045,
    # this is the foreign key to the description of the achievement
    # "nameTextMapHash" corresponds to the keys in `TextMapEN.json` (or other languages)
    # in this case, the corresponding record is "718294045": "Witness the awesome meteorological power of Bennett's phenomenally bad luck."

    "isShow": "SHOWTYPE_HIDE",
    # whether the achievement is hidden in the UI before you achieve it, or not

    "ps5TitleTextMapHash": 3391056799,
    "ttype": "",
    "psTrophyId": "",
    "ps4TrophyId": "",
    "ps5TrophyId": "",
    "icon": "",
    "finishRewardId": 804100,
    "isDeleteWatcherAfterFinish": true,
    "id": 84100,
    "triggerConfig": {
      "triggerType": "TRIGGER_MAIN_COOP_SAVE_POINT_AND",
      "paramList": [
        "103201",
        "201",
        "",
        ""
      ]
    },
    "progress": 1
  },
```


`AchievementGoalExcelConfigData.json` 
-------------------------------------
This is a list of achievement categories, for example "Memories of the Heart", "Challenger: Series II", etc.
Each achievement category is represented by a dict of the following format:


```
  {
    "id": 17,
    # this is the unique key of the achievement category
    # this ID is used by `AchievementExcelConfigData.json` (it is "goalId" in there)

    "orderId": 2,
    # this is the order in which it appears in the achievement UI in game

    "nameTextMapHash": 330935168,
    # this is the foreign key to the name of the achievement category
    # "nameTextMapHash" corresponds to the keys in `TextMapEN.json` (or other languages)
    # for example there is an entry that says "330935168": "Memories of the Heart"

    "iconPath": "UI_AchievementIcon_E001"
  },
```









notes on parsing quests
Example matches:
- "538259095": "An Artist Adrift" (three quests with same name)
- "751312847": "An Artist Adrift" (three quests with same name)
- "1042751263": "An Artist Adrift" (three quests with same name)
- "1674141597": "Complete \"An Artist Adrift.\"" (this is a corresponding achievement)

`\BinOutput\Quest\73296.json`:
has the full quest structure including subquests
example snippet:
```
{
    "id": 73296,
    "type": "WQ",
    "activeMode": "PLAY_MODE_ALL",
    "titleTextMapHash": 538259095,
    "descTextMapHash": 2375123350,
    "luaPath": "Actor/Quest/WQ73296",
    "rewardIdList": [
        173296
    ],
    "subQuests": [
        {
            "subId": 7329601,
            "mainId": 73296,
            "order": 1,
            "descTextMapHash": 1942749364,
            "showType": "QUEST_HIDDEN",
            "finishCond": [
                {
                    "type": "QUEST_CONTENT_TIME_VAR_PASS_DAY",
                    "param": [
                        73295,
```

`BinOutput\QuestBrief\73296.json`:
not sure difference with above, do a diff later

`\ExcelBinOutput\MainQuestExcelConfigData.json`:
snippet, didn't parse yet:
(contains a list of dicts of this structure)
```
  {
    "id": 73296,
    "type": "WQ",
    "titleTextMapHash": 538259095,
    "descTextMapHash": 2375123350,
    "luaPath": "Actor/Quest/WQ73296",
    "suggestTrackMainQuestList": [],
    "rewardIdList": [
      173296
    ],
    "specialShowRewardId": [],
    "specialShowCondIdList": [],
    "OENJEOJBJBO": []
  },
```