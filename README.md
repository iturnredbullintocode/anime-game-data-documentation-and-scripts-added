GenshinData
===========
Repository containing the data for the live version of Genshin Impact.
This repository is a work between [FZFalzar](https://github.com/FZFalzar) and I.

There's a new branch called excel_keys, which properly uses Excel keys for the files.
It's a WIP and some files don't have them, but will add our own soon.


Anna's Bonus Utilities
======================
I happen to be using this repo for a web app I'm making.
In the process of making my web app, I've added documentation, scripts and file compilations for the
original data. I will add data parsing tutorials as well, in the future.

Filesystem Overview
===================

`TextMap`
---------
Contains a giant dict of IDs and their corresponding strings.
For example, in `TextMapEN.json` one of the entries is
`"330935168": "Memories of the Heart"` which is a type of achievement,
and another is  `"3331765120": "*cries in poverty*"` which is a description for an emote.
Each of the IDs and corresponding text can be of wildly varying types of data.
Data types and their structure are defined elsewhere. Having the text strings be all
in a seperate file/database enables the source of truth for text data to not be tied
to actual objects, which allows translations, duplicate data, and language updates
to be handled more smoothly, since only one source of truth needs to be updated for each.


`ExcelBinOutput`
---------------
Contains dicts that depict the herarchy and structure of various objects
such as quests and achievements, and the mappings of text to each object.

Look at `README_ExcelBinOutput.md`, for detailed data.


`Subtitle`
----------
Not parsed yet.


`Readable`
----------
Not parsed yet.


`BinOutput`
-----------
Not parsed yet.


Exclude filter
--------------
Useful for manual searching, to skip the non-english files:
-TextMapCHS.txt,-TextMapCHT.txt,-TextMapDE.txt,-TextMapES.txt,-TextMapFR.txt,-TextMapID.txt,-TextMapIT.txt,-TextMapJP.txt,-TextMapKR.txt,-TextMapPT.txt,-TextMapRU.txt,-TextMapTH.txt,-TextMapTR.txt,-TextMapVI.txt